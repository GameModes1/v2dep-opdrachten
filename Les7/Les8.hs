data Person = Person { name   :: String        -- Record syntax
                     , father :: Maybe Person  -- maakt automatisch functies:
                     , mother :: Maybe Person  -- father :: Person -> Maybe Person
                     } deriving Show
           -- zelfde voor name, mother

rickard = Person "Rickard" Nothing Nothing
ned = Person "Ned" (Just rickard) Nothing
robb = Person "Robb" (Just ned) Nothing
grandfather :: Person -> Maybe Person
grandfather p = father p >>= father

countParents :: Person -> Int
countParents (Person _ Nothing Nothing) = 0
countParents (Person _ (Just _) Nothing) = 2
countParents _ = 1

{- In plaats van
[(+1), (*2)] <*> [3, 4, 5] -- [4, 5, 6, 6, 8, 10]

-- willen we dit:
[(+1), (*2)] <*> [3, 4, 5] -- [3+1, 4*2] -}

-- Gegeven
newtype ZipList a = ZipList [a] deriving Show

-- Geef de instanties voor Functor en Applicative:
instance Functor ZipList where
  fmap f (ZipList zl) = ZipList $ fmap f zl

instance Applicative ZipList where
  (ZipList fs) <*> (ZipList as) = ZipList $ zipWith ($) fs as

