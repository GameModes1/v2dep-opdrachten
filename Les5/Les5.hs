heen :: a -> (a, ())
heen a = (a , ())

terug :: (a, ()) -> a
terug (a, ()) = a

heen1 :: Either a a -> (Bool, a)
heen1 (Left a) = (False, a)
heen1 (Right a) = (True, a)

terug1 :: (Bool, a) -> Either a a
terug1 (False, a) = (Left a)
terug1 (True, a) = (Right a)

uithaak :: (a, Either b c) -> Either(a, b)(a, c)
uithaak (a, Left b) = Left (a, b)
uithaak (a, Right c) = Right (a, c)

inhaak :: Either(a, b)(a, c) -> (a, Either b c)
inhaak (Left (a, b)) = (a, Left b)
inhaak (Right (a, c)) = (a, Right c)