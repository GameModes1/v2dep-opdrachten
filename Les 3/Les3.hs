kanFalen :: Double -> Double -> Either String Double
kanFalen _ 0 = Left "Error: division by zero"
kanFalen n d = Right $ n / d