main :: IO ()
main = do putStrLn "Zeg een zin"
          rev <- getLine
          putStrLn (reverse(rev) ++ " is het omgekeerde")

main2 :: IO ()
main2 = do putNumbLn "Zeg een zin"
           numb <- getLine
           putNumbLn (reverse(numb) ++ " is het omgekeerde")

is_prime :: Int -> Bool
is_prime 1 = False
is_prime 2 = True
is_prime n | (length [x | x <- [2 .. n-1], mod n x == 0]) > 0 = False
                    | otherwise = True