{-|
    Module      : Lib
    Description : Tweede checkpoint voor V2DeP: cellulaire automata
    Copyright   : (c) Brian van der Bijl, 2020
    License     : BSD3
    Maintainer  : brian.vanderbijl@hu.nl

    In dit practicum gaan we aan de slag met 1D cellulaire automata [<https://mathworld.wolfram.com/Rule30.html>].
-}

module Lib where

import Data.Maybe (catMaybes) -- Niet gebruikt, maar deze kan van pas komen...
import Data.List (unfoldr)
import Data.Tuple (swap)

-- Om de state van een cellulair automaton bij te houden bouwen we eerst een set functies rond een `FocusList` type. Dit type representeert een 1-dimensionale lijst, met een
-- enkel element dat "in focus" is. Het is hierdoor mogelijk snel en makkelijk een enkele cel en de cellen eromheen te bereiken.

-- * FocusList

{- | The focussed list [0 1 2 ⟨3⟩ 4 5] is represented as @FocusList [3,4,5] [2,1,0]@. The first element (head) of the first list is focussed, and is easily and cheaply accessible.
 -   The items before the focus are placed in the backwards list in reverse order, so that we can easily move the focus by removing the focus-element from one list and prepending
 -   it to the other.
-}
data FocusList a = FocusList { forward :: [a]
                             , backward :: [a]
                             }
  deriving Show

-- De instance-declaraties mag je voor nu negeren.
instance Functor FocusList where
  fmap = mapFocusList

-- Enkele voorbeelden om je functies mee te testen:
intVoorbeeld :: FocusList Int
intVoorbeeld = FocusList [3,4,5] [2,1,0]

stringVoorbeeld :: FocusList String
stringVoorbeeld = FocusList ["3","4","5"] ["2","1","0"]

-- toList intVoorbeeld ~> [0,1,2,3,4,5]
toList :: FocusList a -> [a]
--toList (FocusList {forward = [a], backward = ay}) =  (a:ay)
--Todo
toList (FocusList fw bw) = reverse bw ++ fw --zet alles uit de forward en backward in een lijst waarbij forward omgekeerd eerst gaat [0,1,2,3,4,5]

--Todo
fromList :: [a] -> FocusList a
fromList a = FocusList {forward = a, backward = []} --pakt de head uit een list en maakt het de focus element in de focusList

-- | Move the focus one to the left
goLeft :: FocusList a -> FocusList a
goLeft (FocusList fw (f:bw)) = FocusList (f:fw) bw --pakt de head uit de focuslist(backward) en zet de in de focuslist(forward)

--Todo
goRight :: FocusList a -> FocusList a
goRight (FocusList (f:fw) bw) = FocusList fw (f:bw)--pakt de head uit de focuslist(forward) en zet de in de focuslist(backward) (andersom dan voorgedaan)

--Todo
leftMost :: FocusList a -> FocusList a
leftMost (FocusList fw []) = (FocusList fw [])--anti-infinity
leftMost (FocusList fw bw) = leftMost (goLeft (FocusList fw bw)) --verplaatst alles van backward naar forward focuslist(naar links) met een loop

--Todo
rightMost :: FocusList a -> FocusList a
rightMost (FocusList fw []) = (FocusList fw [])
rightMost (FocusList fw bw) = leftMost (goRight (FocusList fw bw))--verplaatst alles van forward naar backward focuslist(naar rechts) met een loop

-- De functies goLeft en goRight gaan er impliciet vanuit dat er links respectievelijk rechts een cell gedefinieerd is. De aanroep `goLeft $ fromList [1,2,3]` zal echter crashen
-- omdat er in een lege lijst gezocht wordt: er is niets links. Dit is voor onze toepassing niet handig, omdat we bijvoorbeeld ook de context links van het eerste vakje nodig
-- hebben om de nieuwe waarde van dat vakje te bepalen (en dito voor het laatste vakje rechts).

-- Schrijf en documenteer de functies totalLeft en totalRight die de focus naar links respectievelijk rechts opschuift; als er links/rechts geen vakje meer is, dan wordt een
-- lege (dode) cel teruggeven. Hiervoor gebruik je de waarde `mempty`, waar we met een later college nog op in zullen gaan. Kort gezegd zorgt dit ervoor dat de FocusList ook
-- op andere types blijft werken - je kan dit testen door totalLeft/totalRight herhaaldelijk op de `voorbeeldString` aan te roepen, waar een leeg vakje een lege string zal zijn.

-- [⟨░⟩, ▓, ▓, ▓, ▓, ░]  ⤚goLeft→ [⟨░⟩, ░, ▓, ▓, ▓, ▓, ░]

--Todo
totalLeft :: (Eq a, Monoid a) => FocusList a -> FocusList a
totalLeft (FocusList [x] bw) = (FocusList [mempty] (x:bw))
totalLeft (FocusList fw bw) = goLeft (FocusList fw bw) --verplaats elk element op zichzelf naar links (zonder integer loops)

--Todo
totalRight :: (Eq a, Monoid a) => FocusList a -> FocusList a
totalRight (FocusList [x] bw) = (FocusList [mempty] (x:bw))
totalRight (FocusList (f:fw) bw) = FocusList fw (f:bw) --verplaats elk element op zichzelf naar rechts (zonder integer loops)

--Todo
mapFocusList :: (a -> b) -> FocusList a -> FocusList b
mapFocusList f (FocusList fw bw) = FocusList (map f fw) (map f bw) --maakt eerst FocusList een Forward(list) en Backward(list) en gebruikt een map op iedere list.
-- En zet ze dan terug om in een FocusList

--Todo
zipFocusListWith :: (a -> b -> c) -> FocusList a -> FocusList b -> FocusList c
zipFocusListWith f (FocusList fw bw) (FocusList fw2 bw2)  = FocusList(zipWith (f) fw fw2)(zipWith (f) bw bw2)--maakt eerst FocusList een Forward(list) en Backward(list) (2x) en gebruikt een Zipwidth op iedere list.
                                                                                                             -- En zet ze dan terug om in een FocusList
--Todo
foldFocusList :: (a -> a -> a) -> FocusList a -> a
foldFocusList f (FocusList fw bw) = (foldr1 f (reverse bw)) `f` (foldl1 f fw) -- Focuslist uit elkaar trekken (eerst reverse backward) en elke list folden op de manier waarop ze zijn opgebouwd

-- | Test function for the behaviour of foldFocusList.
testFold :: Bool
testFold = and [ foldFocusList (+) intVoorbeeld     == 15
               , foldFocusList (-) intVoorbeeld     == 7
               , foldFocusList (++) stringVoorbeeld == "012345"
               ]


-- | A cell can be either on or off, dead or alive. What basic type could we have used instead? Why would we choose to roll our own equivalent datatype?
data Cell = Alive | Dead deriving (Show, Eq)

-- De instance-declaraties mag je voor nu negeren.
instance Semigroup Cell where
  Dead <> x = x
  Alive <> x = Alive

instance Monoid Cell where
  mempty = Dead

-- | The state of our cellular automaton is represented as a FocusList of Cells.
type Automaton = FocusList Cell

-- | Start state, per default, is a single live cell.
start :: Automaton
start = FocusList [Alive] []

-- | Alternative start state with 5 alive cells, for shrinking rules.
fiveAlive :: Automaton
fiveAlive = fromList $ replicate 5 Alive

-- | A rule [<https://mathworld.wolfram.com/Rule30.html>] is a mapping from each possible combination of three adjacent cells to the associated "next state".
type Context = [Cell]
type Rule = Context -> Cell

-- * Rule Iteration

-- TODO
safeHead :: a        -- ^ Default value
         -> [a]      -- ^ Source list
         -> a
safeHead a [] = a
safeHead a l = head l --list != empty dan geeft head, anders list = a

-- TODO
takeAtLeast :: Int   -- ^ Number of items to take
            -> a     -- ^ Default value added to the right as padding
            -> [a]   -- ^ Source list
            -> [a]
takeAtLeast 0 extravalue _ = []--antiloop
takeAtLeast n extravalue [] = (extravalue:takeAtLeast (n-1) extravalue [])  --als n > length list geef dan list + ((list - n) * extravalue)
takeAtLeast n extravalue (x:xs) = (x:takeAtLeast (n-1) extravalue xs) --geef list met alleen N elements van list



-- TODO ~DUS takeAtLeast gebruiken om een automaton (Dead|Alive FocusList) terug te geven~
context :: Automaton -> Context
context (FocusList fw bw) = takeAtLeast 1 Dead bw ++ takeAtLeast 2 Dead fw --geeft door middel van TakeAtLeast functie de focus (bw 1) en de (reverse) van de forward

-- TODO ~DUS elke cell van een automaton uitbreiden met Dead~
expand :: Automaton -> Automaton
expand (FocusList fw bw) = FocusList (fw ++ [Dead]) (bw ++ [Dead]) --haalt eerst forward en backward uit de focuslist en geeft een Dead cell aan ieder om ze daarna weer terug te zetten


-- | A sequence of Automaton-states over time is called a TimeSeries.
type TimeSeries = [Automaton]

-- TODO Voorzie onderstaande functie van interne documentatie, d.w.z. zoek uit en beschrijf hoe de recursie verloopt. Zou deze functie makkelijk te schrijven zijn met behulp van
-- de hogere-orde functies die we in de les hebben gezien? Waarom wel/niet?

-- | Iterate a given rule @n@ times, given a start state. The result will be a sequence of states from start to @n@.
iterateRule :: Rule          -- ^ The rule to apply
            -> Int           -- ^ How many times to apply the rule
            -> Automaton     -- ^ The initial state
            -> TimeSeries
iterateRule r 0 s = [s] --als n = 0 dan print lijst S (er wordt dus met n gerekend om *0 en /0 te voorkomen)
iterateRule r n s = s : iterateRule r (pred n) (fromList $ applyRule $ leftMost $ expand s)
--(haalt head van list, forward, rest backward (* * (verplaatst backward naar forward (Voegt dead cell toe)))
  where applyRule :: Automaton -> Context
        applyRule (FocusList [] bw) = []
        applyRule z = r (context z) : applyRule (goRight z)

-- | Convert a time-series of Automaton-states to a printable string.
showPyramid :: TimeSeries -> String
showPyramid zs = unlines $ zipWith showFocusList zs $ reverse [0..div (pred w) 2]
  where w = length $ toList $ last zs :: Int
        showFocusList :: Automaton -> Int -> String
        showFocusList z p = replicate p ' ' <> concatMap showCell (toList z)
        showCell :: Cell -> String
        showCell Dead  = "░"
        showCell Alive = "▓"

-- TODO
rule30 :: Rule --met de hints van David en Guy, was dit patroon makkelijk te ontcijferen door op de wolfram website alle mogelijkheden te checken,
-- waarbij je kan zien dat alle 8 (9 - [Alles Alive naar Alive) dus 8) mogelijkheden je makkelijk de basis kreeg. En die kan je afkorten met "_".
rule30 [Alive, Alive, _] = Dead
rule30 [Alive, Dead, Alive] = Dead
rule30 [Alive, Dead, Dead] = Alive
rule30 [Dead, Alive, _] = Alive
rule30 [Dead, Dead, Alive] = Alive
rule30 [Dead, Dead, Dead] = Dead

--Basis:
--rule30 [Alive, Alive, Alive] = Dead
--rule30 [Alive, Alive, Dead] = Dead
--rule30 [Alive, Dead, Alive] = Dead
--rule30 [Alive, Dead, Dead] = Alive
--rule30 [Dead, Alive, Alive] = Alive
--rule30 [Dead, Alive, Dead] = Alive
--rule30 [Dead, Dead, Alive] = Alive
--rule30 [Dead, Dead, Dead] = Dead


-- Je kan je rule-30 functie in GHCi testen met het volgende commando:
-- putStrLn . showPyramid . iterateRule rule30 15 $ start

-- De verwachte uitvoer is dan:
{-             ▓
              ▓▓▓
             ▓▓░░▓
            ▓▓░▓▓▓▓
           ▓▓░░▓░░░▓
          ▓▓░▓▓▓▓░▓▓▓
         ▓▓░░▓░░░░▓░░▓
        ▓▓░▓▓▓▓░░▓▓▓▓▓▓
       ▓▓░░▓░░░▓▓▓░░░░░▓
      ▓▓░▓▓▓▓░▓▓░░▓░░░▓▓▓
     ▓▓░░▓░░░░▓░▓▓▓▓░▓▓░░▓
    ▓▓░▓▓▓▓░░▓▓░▓░░░░▓░▓▓▓▓
   ▓▓░░▓░░░▓▓▓░░▓▓░░▓▓░▓░░░▓
  ▓▓░▓▓▓▓░▓▓░░▓▓▓░▓▓▓░░▓▓░▓▓▓
 ▓▓░░▓░░░░▓░▓▓▓░░░▓░░▓▓▓░░▓░░▓
▓▓░▓▓▓▓░░▓▓░▓░░▓░▓▓▓▓▓░░▓▓▓▓▓▓▓ -}

-- * Rule Generation

-- TODO
-- Je mag dit met de hand uitschrijven, maar voor extra punten kun je ook een lijst-comprehensie of andere slimme functie verzinnen.

inputs :: [Context]
inputs = undefined
--Handmatig:
--rule30 [Alive, Alive, Alive] = Dead
--rule30 [Alive, Alive, Dead] = Dead
--rule30 [Alive, Dead, Alive] = Dead
--rule30 [Alive, Dead, Dead] = Alive
--rule30 [Dead, Alive, Alive] = Alive
--rule30 [Dead, Alive, Dead] = Alive
--rule30 [Dead, Dead, Alive] = Alive
--rule30 [Dead, Dead, Dead] = Dead

--


-- | If the given predicate applies to the given value, return Just the given value; in all other cases, return Nothing.
guard :: (a -> Bool) -> a -> Maybe a
guard p x
      | p x = Just x
      | otherwise = Nothing

-- TODO Deze functie converteert een Int-getal naar een binaire representatie [Bool]. Zoek de definitie van `unfoldr` op met Hoogle en `guard` in Utility.hs; `toEnum` converteert
-- een Int naar een ander type, in dit geval 0->False en 1->True voor Bool. Met deze kennis, probeer te achterhalen hoe de binary-functie werkt en documenteer dit met Haddock.
binary :: Int -> [Bool]
binary = map toEnum . reverse . take 8 . (++ repeat 0)
       . unfoldr (guard (/= (0,0)) . swap . flip divMod 2)

-- TODO Schrijf en documenteer een functie mask die, gegeven een lijst Booleans en een lijst elementen alleen de elementen laat staan die (qua positie) overeenkomen met een True.
-- Je kan hiervoor zipWith en Maybe gebruiken (check `catMaybes` in Data.Maybe) of de recursie met de hand uitvoeren.
mask :: [Bool] -> [a] -> [a]
mask = undefined

-- TODO Combineer `mask` en `binary` met de library functie `elem` en de eerder geschreven `inputs` tot een rule functie. Denk eraan dat het type Rule een short-hand is voor een
-- functie-type, dus dat je met 2 argumenten te maken hebt. De Int staat hierbij voor het nummer van de regel, dat je eerst naar binair moet omrekenen; de Context `input` is
-- waarnaar je kijkt om te zien of het resultaat met de gevraagde regel Dead or Alive is. Definieer met `where` subset van `inputs` die tot een levende danwel dode cel leiden.
-- Vergeet niet je functie te documenteren.
rule :: Int -> Rule
rule n input = undefined

{- Je kan je rule-functie in GHCi testen met variaties op het volgende commando:

   putStrLn . showPyramid . iterateRule (rule 18) 15 $ start

                  ▓
                 ▓░▓
                ▓░░░▓
               ▓░▓░▓░▓
              ▓░░░░░░░▓
             ▓░▓░░░░░▓░▓
            ▓░░░▓░░░▓░░░▓
           ▓░▓░▓░▓░▓░▓░▓░▓
          ▓░░░░░░░░░░░░░░░▓
         ▓░▓░░░░░░░░░░░░░▓░▓
        ▓░░░▓░░░░░░░░░░░▓░░░▓
       ▓░▓░▓░▓░░░░░░░░░▓░▓░▓░▓
      ▓░░░░░░░▓░░░░░░░▓░░░░░░░▓
     ▓░▓░░░░░▓░▓░░░░░▓░▓░░░░░▓░▓
    ▓░░░▓░░░▓░░░▓░░░▓░░░▓░░░▓░░░▓
   ▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓

   putStrLn . showPyramid . iterateRule (rule 128) 10 $ fiveAlive

               ▓▓▓▓▓
              ░░▓▓▓░░
             ░░░░▓░░░░
            ░░░░░░░░░░░
           ░░░░░░░░░░░░░
          ░░░░░░░░░░░░░░░
         ░░░░░░░░░░░░░░░░░
        ░░░░░░░░░░░░░░░░░░░
       ░░░░░░░░░░░░░░░░░░░░░
      ░░░░░░░░░░░░░░░░░░░░░░░
     ░░░░░░░░░░░░░░░░░░░░░░░░░

   Als het goed is zal `stack run` nu ook werken met de voorgeschreven main functie; experimenteer met verschillende parameters en zie of dit werkt.
-}
