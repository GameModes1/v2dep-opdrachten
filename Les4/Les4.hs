import Prelude hiding (map, foldr, foldr1, foldl, foldl1, zipWith)

map :: [Int] -> [Int]
map f [] = []
map f (x:xs) = (f x) : (map f xs)

altmap l = (\f -> map f l)