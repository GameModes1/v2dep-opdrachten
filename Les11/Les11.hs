import Control.Monad.Trans.State
import Control.Monad
import Control.Applicative
import Data.List (uncons)

type Parser = (StateT String Maybe)

pSpace :: Parser Int
pSpace = do input <- uncons <$> get
            case input of
              Just (' ', t) -> put t >> return 1
              _             -> mzero

pSpaces :: Parser Int
pSpaces = do firstP <- pSpace
             --restP <- pSpaces <|> return 0
             restP <- mplus pSpaces (return 0)
             return $ firstP + restP