module Lib
    ( ex1, ex2, ex3, ex4, ex5, ex6, ex7
    ) where

-- Schrijf een functie die de som van een lijst getallen berekent, net als de `product` functie uit de les.
ex1 :: [Int] -> Int
ex1 [x] = x
ex1 (x:xs) = x + ex1 xs --loop-ed door de lijst en bij elk element telt het zich op in een variabele

-- Schrijf een functie die alle elementen van een lijst met 1 ophoogt; bijvoorbeeld [1,2,3] -> [2,3,4]. Het is voor deze opgave nog niet de bedoeling dat je hogere-orde functies gebruikt.
ex2 :: [Int] -> [Int]
ex2 [] = []
ex2 (x:xs) = x + 1 : ex2 (xs) --loop-ed door de lijst heen en voegt elk element toe nadat het met 1 verhoogt is

-- Schrijf een functie die alle elementen van een lijst met -1 vermenigvuldigt; bijvoorbeeld [1,-2,3] -> [-1,2,-3]. Het is voor deze opgave nog niet de bedoeling dat je hogere-orde functies gebruikt.
ex3 :: [Int] -> [Int]
ex3 [] = []
ex3 (x:xs) = x * (-1) : ex3 (xs) --loop-ed door de lijst heen en voegt elk element toe nadat het met -1 vermenigvuldigt is

-- Schrijf een functie die twee lijsten aan elkaar plakt, dus bijvoorbeeld [1,2,3] en [4,5,6] combineert tot [1,2,3,4,5,6]. Maak hierbij geen gebruik van de standaard-functies, maar los het probleem zelf met (expliciete) recursie op. Hint: je hoeft maar door een van beide lijsten heen te lopen met recursie.
ex4 :: [Int] -> [Int] -> [Int]
ex4 [] lijst = lijst
ex4 (x:xs) lijst = x : ex4 xs lijst --voegt eerst de 1ste lijst toe ~1 : (2 : (3 : [4,5,6])~ en dan dmv regel 22 ziet dat hij de 1ste leeg is voegt dan de 2de lijst toe ~(4 : (5 : (6 :[])~


-- Schrijf een functie die twee lijsten paarsgewijs bij elkaar optelt, dus bijvoorbeeld [1,2,3] en [4,5,6] combineert tot [1+4, 2+5, 3+6] oftewel [5,7,9]. Het is voor deze opgave nog niet de bedoeling dat je hogere-orde functies gebruikt.
ex5 :: [Int] -> [Int] -> [Int]
ex5 list [] = list
ex5 lst1 lst2 = head lst1 + head lst2 :ex5 (tail lst1) (tail lst2) --loop-ed door 2 lijsten tegelijk en telt bij elke getal dat het tegenkomt bijelkaar op (head + head) tot aan het einde van de list (tail)


-- Schrijf een functie die twee lijsten paarsgewijs met elkaar vermenigvuldigt, dus bijvoorbeeld [1,2,3] en [4,5,6] combineert tot [1*4, 2*5, 3*6] oftewel [4,10,18]. Het is voor deze opgave nog niet de bedoeling dat je hogere-orde functies gebruikt.
ex6 :: [Int] -> [Int] -> [Int]
ex6 list [] = list
ex6 lst1 lst2 = head lst1 * head lst2 :ex6 (tail lst1) (tail lst2) --loop-ed door 2 lijsten tegelijk en vermenigvuldig bij elke getal dat het tegenkomt bijelkaar op (head + head) tot aan het einde van de list (tail)


-- Schrijf een functie die de functies uit opgave 1 en 6 combineert tot een functie die het inwendig product uitrekent. Bijvoorbeeld: `ex7 [1,2,3] [4,5,6]` -> 1*4 + 2*5 + 3*6 = 32.
ex7 :: [Int] -> [Int] -> Int
ex7 lst1 lst2 = ex1 $ ex6 (lst1)(lst2) --voert eerst opdracht 6 uit met ex6(lst1)(lst2) en gebruikt de uitkomst als input voor ex1

