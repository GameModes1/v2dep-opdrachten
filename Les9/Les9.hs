type Stand = (String, Integer, Integer, String)

tussenstand :: Stand
tussenstand = ("FC Groningen", 0, 0, "PSV")

toonStand :: Stand -> String  -- Helperfunctie, om de stand te laten zien
toonStand (tt, ts, us, ut) = concat [tt, "  ", show ts, "-", show us, "  ", ut]

doelpunt :: Bool -> Stand -> (String, Stand)
doelpunt thuisofuit (tt, ts, us, ut) =
  let ts' = if thuisofuit then ts+1 else ts
      us' = if thuisofuit then us else us+1
      leider = if ts' > us' then tt else (if ts' < us' then ut else "Gelijkspel")
  in (leider, (tt, ts', us', ut))

thuisDoelpunt :: Stand -> (String, Stand)
thuisDoelpunt = doelpunt True

uitDoelpunt :: Stand -> (String, Stand)
uitDoelpunt = doelpunt False


fcg_psv :: Stand -> (String, Stand)
fcg_psv s =
    let (doel, s') = uitDoelpunt s
        (doel', s'') = thuisDoelpunt s'
        (doel'', s''') = uitDoelpunt s''
    in uitDoelpunt s'''



get :: State s s
get = State $ \s -> (s, s)

put :: s -> State s ()
put newState = State $ \_ -> ((), newState)

modify :: (s -> s) -> State s ()
modify f = do s <- get
              put $ f s

runState :: State s a -> s -> (a, s) -- Van de constructor

evalState :: State s a -> s -> a
evalState = fmap fst . runState      -- Laatste resultaat

execState :: State s a -> s -> s
execState = fmap snd . runState      -- Eindstate