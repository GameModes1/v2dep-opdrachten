-- Functies voor State (niet aankomen)

import Control.Monad (ap)
import Control.Applicative (Applicative(..), liftA)

newtype State s a = State {runState :: s -> (a, s)}

instance Functor (State s) where
    fmap = liftA

instance Applicative (State s) where
    {-# INLINE pure #-}
    pure x = State $ \ s -> (x, s)
    (<*>) = ap

instance Monad (State s) where
    {-# INLINE return #-}
    {-# INLINE (>>=) #-}
    return = pure
    m >>= k = State $ \ s -> case runState m s of
        (x, s') -> runState (k x) s'

get :: State s s
get = State $ \s -> (s, s)

put :: s -> State s ()
put newState = State $ \_ -> ((), newState)

modify :: (s -> s) -> State s ()
modify f = do s <- get
              put $ f s

-- runState :: State s a -> s -> (a, s) -- Van de constructor

evalState :: State s a -> s -> a
evalState = fmap fst . runState      -- Laatste resultaat

execState :: State s a -> s -> s
execState = fmap snd . runState      -- Eindstate

-- Functies voor ons programma

type Stand = (String, Integer, Integer, String)

tussenstand :: Stand
tussenstand = ("Emmen", 3, 4, "VVV-Venlo")

toonStand :: Stand -> String  -- Helperfunctie, om de stand te laten zien
toonStand (tt, ts, us, ut) = concat [tt, "  ", show ts, "-", show us, "  ", ut]

incrementSnd :: State Stand ()
incrementSnd = modify (\(a,b,c,d) -> (a, b+1, c, d))

incrementThr :: State Stand ()
incrementThr = modify (\(a,b,c,d) -> (a, b, c+1, d))

doelpunt'' :: Bool -> State Stand String
doelpunt'' thuisofuit = do if thuisofuit then incrementSnd else incrementThr
                           (tt, ts, us, ut) <- get
                           return (case (compare ts us) of
                            LT -> ut
                            EQ -> "Gelijkspel"
                            GT -> tt)

thuisDoelpunt' :: State Stand String
thuisDoelpunt' = doelpunt'' True

uitDoelpunt' :: State Stand String
uitDoelpunt' = doelpunt'' False

fcg_psv' :: State Stand String
fcg_psv' = do uitDoelpunt'
              thuisDoelpunt'
              uitDoelpunt'
              uitDoelpunt'
